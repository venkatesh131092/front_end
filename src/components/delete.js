import React from 'react';
import Axios from 'axios';

class Delete extends React.Component {
    state = {
        password: '',
        mail: '',
        firstname: '',
        lastname: '',
        address: '',
        Login: 0,
        loggedin: false,
        valid: false
    };
    
    onDeleteDetails = async (event,props) => {
        event.preventDefault();
        console.log("coming......1");
        await Axios.delete(`http://192.168.43.207:5000/delete`, {
            data: {
                token: localStorage.getItem("token")
            }
        }).then(function (response) {
            
            alert(response.data.message)
            localStorage.removeItem("token");
            window.location.href = '/signin'
            console.log(response);
            
        })
            .catch(function (error) {
                console.log(error);
            });
    };
    render() {
        return (
            <input type="submit" value="are you sure" onClick={this.onDeleteDetails} />
        );
}
}

export default Delete;