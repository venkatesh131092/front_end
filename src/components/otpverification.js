import React from 'react';
import Axios from 'axios';
// import { BrowserRouter as Router, Route } from 'react-router-dom';
//import UserDetails from './userdetails';
// import Update from './update';
// import Delete from './delete';
// import Logout from './logout';
//  import history from '../history';
class otpVerification extends React.Component {
    state = {
        otp: ''
    };
    verification = async (event) => {
        event.preventDefault();
        await Axios({
            method: 'post',
            url: 'http://192.168.43.207:5000/otpverify',
            data: {
                otp: this.state.otp
            },
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }


        })
            .then(function (response) {
                if (response.data.valid === true) {
                    alert("OTP is verified");
                    window.location.href = '/signin'

                }
                else {
                    console.log( "OTP is not valid \n");
                    alert("You entered wrong otp");
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {

        return (
            <div className="container">
                <div className="reg">
                    <label className="heading"><h1>Enter OTP</h1></label>
                    <form className="login" >
                        OTP:<input type="text" value={this.state.otp} onChange={e => this.setState({ otp: e.target.value })} /><br />
                        <div>
                            <input type="submit" value="verify" onClick={this.verification} />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default otpVerification;