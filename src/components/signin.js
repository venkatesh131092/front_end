import React from 'react';
import Axios from 'axios';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import UserDetails from './userdetails';
import Update from './update';
import Delete from './delete';
import Logout from './logout';
import history from '../history';
class SignIn extends React.Component {
    state = {
        password: '',
        mail: '',
        firstname: '',
        lastname: '',
        address: '',
        Login: 0,
        loggedin: false,
        valid: false
    };
    onTokenVerify = async (event) => {
        event.preventDefault();
        await Axios({
            method: 'get',
            url: 'http://192.168.43.207:5000/tokenvalidation',
            data: {
                token:localStorage.getItem("token")
            },
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }


        })
            .then(function (response) {
                if (response.data.valid) {
                   this.setState({valid:true})

                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }


    onGetDetails = async (event) => {
        event.preventDefault();
        await Axios({
            method: 'post',
            url: 'http://192.168.43.207:5000/signin',
            data: {
                email: this.state.mail,
                password: this.state.password,
            },
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }


        })
            .then(function (response) {
                if (response.data.message === "Login successful\n") {
                    localStorage.setItem("token", response.data.token);
                    console.log(response.data.message, "  \n", response.data.token);
                    console.log("this is token", localStorage.getItem("token"));
                    alert(response.data.message);
                    window.location.href = '/userdetails'

                }
                else {
                    console.log(response.data.message, "  \n");
                    alert(response.data.message);
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    render() {
        if (localStorage.getItem("token")) {
            return (
            <div className="ui container">
                <Router history={history}>
                    <div>
                        <UserDetails />
                        <Route path="/delete" exact component={Delete} />
                        <Route path="/logout" exact component={Logout} />
                        <Route path="/update" exact component={Update} />
                    </div>
                </ Router>
            </div>)
        }
        else {
            return (
                <div className="container">
                    <div className="reg">
                        <label className="heading"><h1>Login Form</h1></label>
                        <form className="login" >
                            Mail ID:&nbsp;&nbsp;<input type="text" value={this.state.mail} onChange={e => this.setState({ mail: e.target.value })} /><br />
                            Password:<input type="password" value={this.state.password} onChange={e => this.setState({ password: e.target.value })} /><br />
                            <div>
                                <input type="submit" value="Login" onClick={this.onGetDetails} />
                            </div>
                        </form>
                    </div>
                </div>
            );
        }
    }
}

export default SignIn;