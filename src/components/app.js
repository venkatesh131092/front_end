import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import SignUp from './signup';
import SignIn from './signin';
import Header from './header';
import OtpVerification from './otpverification';
import history from '../history';
import '../styles/styles.css';
class App extends React.Component{
    render(){
        return (
            <div className="ui container">
            <Router  history={history}>
                <div>
                    <Header />
                    <Route path="/signin" exact strict component={SignIn} />
                    <Route path="/signup" exact strict component={SignUp} />
                    <Route path="/otpverification" component={OtpVerification} />
                </div>
            </ Router>
        </div>
        );
    }
}

export default App;