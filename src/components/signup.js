import React from 'react';
import Axios from 'axios';
import { BrowserRouter as Router, Route } from 'react-router-dom';
//import { Redirect } from 'react-router';
import history from '../history';
import UserDetails from './userdetails';
// import SignIn from './signin';
import Update from './update';
import Delete from './delete';
import Logout from './logout';
class SignUp extends React.Component {
    state = {
        password: '',
        mail: '',
        firstname: '',
        lastname: '',
        address: '',
        mobile: '',
        Login: 0,
        loggedin: false,
        valid: false
    };
    onTokenVerify = async (event) => {
        event.preventDefault();
        await Axios({
            method: 'get',
            url: 'http://192.168.43.207:5000/tokenvalidation',
            data: {
                token: localStorage.getItem("token")
            },
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }


        })
            .then(function (response) {
                if (response.data.valid) {
                    this.setState({ valid: true })
                    window.location.href = '/userdetails'
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    onPostDetails = async (event) => {
        event.preventDefault();
        console.log("signup post method")
        await Axios({
            method: 'post',
            url: 'http://192.168.43.207:5000/signup',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            },
            data: {
                email: this.state.mail,
                password: this.state.password,
                mobile: this.state.mobile
            }

        }).then(function (response) {
            if (response) {
                alert(response.data.message);
                console.log(response.data.message)
                console.log(response);
                window.location.href = '/signin'
                // history.push(`/signin`);
            }
        })
            .catch(function (error) {
                console.log("coming to sign up catch method")
                console.log(error);
            });
    };
    render() {
        if (localStorage.getItem("token")) {
            return (
                <div className="ui container">
                    <Router history={history}>
                        <div>
                            <UserDetails />
                            <Route path="/delete" exact component={Delete} />
                            <Route path="/logout" exact component={Logout} />
                            <Route path="/update" exact component={Update} />
                        </div>
                    </ Router>
                </div>
            )

        }
        else {
            return (
                <div className="container">
                    <div className="reg">
                        <label className="heading"><h1>Registration Form</h1></label>
                        <form className="login" >
                            Mail ID:&nbsp;&nbsp;&nbsp;<input type="text" value={this.state.mail} onChange={e => this.setState({ mail: e.target.value })} /><br />
                            Password:<input type="password" value={this.state.password} onChange={e => this.setState({ password: e.target.value })} /><br />
                            Mobile:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" maxlength="10" pattern="\d{10}"  value={this.state.mobile} onChange={e => this.setState({ mobile: e.target.value })} /><br />
                            <div>
                                <input type="submit" value="Register" onClick={this.onPostDetails} />
                            </div>
                        </form>
                    </div>
                </div>
            );


        }
    }
}

export default SignUp;