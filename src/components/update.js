import React from 'react';
import Axios from 'axios';

class Update extends React.Component {
    state = {
        password: '',
        mail: '',
        firstname: '',
        lastname: '',
        address: '',
        Login: 0,
        loggedin: false,
        valid: false
    };

    onUpdateDetails = async (event) => {
        event.preventDefault();
        //var token=localStorage.getItem("venky");

        console.log("You entered mail", this.state.mail);
        await Axios.put(`http://192.168.43.207:5000/update`, {
            "firstname": this.state.firstname,
            "lastname": this.state.lastname,
            "address": this.state.address,
            token: localStorage.getItem("token")
        }).then(function (response) {
            if (response){
                alert("Email   :" + response.data.data + "\nFirstname:" + response.data.data.$set.userFirstName+ "\nlastname:" + response.data.data.$set.userLastName + "\naddress:" + response.data.data.$set.userAddress)
                window.location.href = '/userdetails'
            }
            console.log(response);
        })
            .catch(function (error) {
                console.log(error);
            });

    };
    render() {
        return (
            <div className="container">
                <div className="reg">
                    <label className="heading"><h1>Update Form</h1></label>
                    <form className="login" >
                        {/* <Mail mail={match.params.mail}/> */}
                        First Name:&nbsp;&nbsp;<input type="text" value={this.state.firstname} onChange={e => this.setState({ firstname: e.target.value })} /><br />
                        Last Name:<input type="text" value={this.state.lastname} onChange={e => this.setState({ lastname: e.target.value })} /><br />
                        Address:<input type="text" value={this.state.address} onChange={e => this.setState({ address: e.target.value })} /><br />
                        <div>

                            <input type="submit" value="Update" onClick={this.onUpdateDetails} />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Update;